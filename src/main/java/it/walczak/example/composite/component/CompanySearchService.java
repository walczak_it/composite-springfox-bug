package it.walczak.example.composite.component;

import it.walczak.example.composite.model.Company;
import it.walczak.example.composite.model.filter.EntityCompositeFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Service
@RestController
@RequestMapping("/api/companies")
public class CompanySearchService {
    
    private CompanyRepository repository;

    @Autowired
    public CompanySearchService(CompanyRepository repository) {
        this.repository = repository;
    }
    
    @GetMapping
    public Page<Company> findAll(Pageable pageable) {
        return repository.findAll(pageable);
    }
    
    @PostMapping("searches")
    public Page<Company> search(Pageable pageable, @RequestBody EntityCompositeFilter<Company> filter) {
        return repository.findAll(filter, pageable);
    }
}
