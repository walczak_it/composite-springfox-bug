package it.walczak.example.composite.model.filter;

import java.util.Iterator;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import lombok.Getter;
import lombok.Setter;

public class BooleanFilter<T> implements InMemoryFilter<T>, EntityFilter<T> {
    
    @Getter
    @Setter
    private List<String> attributePath;
    
    @Getter
    @Setter
    private boolean value;

    @Override
    public boolean matches(T obj) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        Iterator<String> pathIterator = attributePath.iterator();
        Path attribute = root.get(pathIterator.next());
        while (pathIterator.hasNext()) {
            attribute = attribute.get(pathIterator.next());
        }
        return criteriaBuilder.equal(attribute, value);
    }
    
}
