package it.walczak.example.composite.model.filter;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

public abstract class CompositeFilter<T, F extends Filter<T>> implements Filter<T>{
    
    @Getter
    @Setter
    private Operator operator;
    
    @Getter
    @Setter
    private List<F> filters;
    
    public static enum Operator {
        AND, OR;
    }
}
