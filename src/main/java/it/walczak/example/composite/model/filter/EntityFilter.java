package it.walczak.example.composite.model.filter;

import org.springframework.data.jpa.domain.Specification;

public interface EntityFilter<T> extends Specification<T>, Filter<T> {
    
}
