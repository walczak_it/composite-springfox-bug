package it.walczak.example.composite.model;

import java.util.UUID;
import javax.persistence.Entity;
import javax.persistence.Id;
import lombok.Getter;
import lombok.Setter;

@Entity
public class Company {
    
    @Id
    @Getter
    @Setter
    private UUID id;
    
    @Getter
    @Setter
    private String name;
    
    @Getter
    @Setter
    private boolean active;

    public Company() { }

    public Company(String name, boolean active) {
        this.id = UUID.randomUUID();
        this.name = name;
        this.active = active;
    }
}
