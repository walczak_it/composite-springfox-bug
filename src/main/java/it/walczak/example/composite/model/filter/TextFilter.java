package it.walczak.example.composite.model.filter;

import javax.persistence.criteria.*;
import java.util.Iterator;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

public class TextFilter<T> implements InMemoryFilter<T>, EntityFilter<T> {
    
    @Getter
    @Setter
    private List<String> attributePath;
    
    @Getter
    @Setter
    private Operator operator;
    
    @Getter
    @Setter
    private String value;
    
    public static enum Operator {
        LIKE //, .... more text operators
    }

    public TextFilter() {
    }

    public TextFilter(List<String> attributePath, Operator operator, String value) {
        this.attributePath = attributePath;
        this.operator = operator;
        this.value = value;
    }

    @Override
    public boolean matches(T obj) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        Iterator<String> pathIterator = attributePath.iterator();
        Path attribute = root.get(pathIterator.next());
        while (pathIterator.hasNext()) {
            attribute = attribute.get(pathIterator.next());
        }
        switch (operator) {
            case LIKE : return criteriaBuilder.like(attribute, "%" + value + "%");
            default : throw new IllegalStateException("Unknown operator " + operator);
        }
    }
}
