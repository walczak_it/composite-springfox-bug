package it.walczak.example.composite.model.filter;

public interface InMemoryFilter<T> extends Filter<T> {
    
    boolean matches(T obj);
}
