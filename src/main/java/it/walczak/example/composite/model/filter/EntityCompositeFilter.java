package it.walczak.example.composite.model.filter;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

public class EntityCompositeFilter<T> extends CompositeFilter<T, EntityFilter<T>> implements EntityFilter<T> {

    @Override
    public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        Predicate[] predicates = getFilters().stream()
                .map(filter -> filter.toPredicate(root, query, criteriaBuilder))
                .toArray(Predicate[]::new);
        switch (getOperator()) {
            case AND : return criteriaBuilder.and(predicates);
            case OR : return criteriaBuilder.and(predicates);
            default : throw new IllegalStateException("Unexpected operator " + getOperator());
        }
    }
}
