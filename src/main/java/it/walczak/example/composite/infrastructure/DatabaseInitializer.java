package it.walczak.example.composite.infrastructure;

import it.walczak.example.composite.component.CompanyRepository;
import it.walczak.example.composite.model.Company;
import java.util.Arrays;
import javax.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@Slf4j
public class DatabaseInitializer {
    
    private CompanyRepository companyRepository;

    @Autowired
    public DatabaseInitializer(CompanyRepository companyRepository) {
        this.companyRepository = companyRepository;
    }
    
    @PostConstruct
    @Transactional
    public void initialize() {
        log.info("Initializing database");
        companyRepository.saveAll(
                Arrays.asList(
                        new Company("Atrix", true),
                        new Company("Beator", true),
                        new Company("Citrix", false)
                )
        );
    }
}
